const mongoose = require('mongoose');

const BandaSchema = new mongoose.Schema({
    nome:{
        type: String,
        required: true
    },
    descricao:{
        type: String,
        required: true
    },
    horario:{
        type: String,
        required: true
    },
    imagem:{
        type: String,
        required: true
    },
    video:{
        type: String,
        required: true
    }
}, {timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}});

module.exports = mongoose.model('bandas', BandaSchema);

