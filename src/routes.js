const express = require('express');
const routes = express.Router();
const BandasController = require('./controllers/BandasController');

routes.get('/banda', BandasController.index );
routes.post('/banda', BandasController.create );
routes.delete('/banda/:index', BandasController.delete );
routes.put('/banda/:index', BandasController.update );

module.exports = routes;