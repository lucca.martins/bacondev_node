const BandaModel = require('../models/BandaModel');

module.exports = {
    async index(request, response) {
        try{
            const bandas = await BandaModel.find();
            return response.json(bandas);
        }catch(err){
            return response.status(400).send();
        }
    },

    async create(request, response) {

        try{
            const {
                nome,
                descricao,
                horario,
                imagem,
                video
            } = request.body;
        
            await BandaModel.create({
                nome,
                descricao,
                horario,
                imagem,
                video
            });
        
            return response.status(201).send();
    
        }catch(err){
            return response.status(400).send();
        }
    },
    async delete(request, response) {
        try{
            const idBanda = request.params.index;
            await BandaModel.deleteOne({_id: idBanda});
            return response.status(200).send('Banda deletada');
        }catch(err){
            return response.status(400).send();
        }
    },
    async update(request, response) {
        try{
            const {
                nome,
                descricao,
                horario,
                imagem,
                video
            } = request.body;
    
            const idBanda = request.param.index;
    
            await BandaModel.findOneAndUpdate(
                {_id: idBanda},
                {
                    nome,
                    descricao,
                    horario,
                    imagem,
                    video
                }
            )
    
            return response.status(200).send('Banda Atualizada');
        }
        catch(err){
            return response.status(400).send();
        }
    }
}